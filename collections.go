package mk

import (
	"time"

	"github.com/fatih/structs"
	"github.com/gertd/go-pluralize"
	changecase "github.com/ku/go-change-case"
	"go.mongodb.org/mongo-driver/mongo"
)

type Schema struct {
	CreatedAt time.Time `json:"created_at,omitempty" bson:"created_at,omitempty" faker:"-"`
	UpdatedAt time.Time `json:"updated_at,omitempty" bson:"updated_at,omitempty" faker:"-"`
}

func (m mongoKit) InitCollection(i interface{}, replace ...string) MongoKit {
	var collectionName string

	if len(replace) != 0 {
		collectionName = replace[0]
	} else {
		plural := pluralize.NewClient()
		collectionName = changecase.Snake(plural.Plural(structs.Name(i)))
	}
	m.collection = m.db.Collection(collectionName)
	return m
}

func (m mongoKit) Collection() *mongo.Collection {
	return m.collection
}

func SchemaCreated() Schema {
	return Schema{
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
}

func SchemaUpdated(createdAt time.Time) Schema {
	if !createdAt.IsZero() {
		return Schema{
			CreatedAt: createdAt,
			UpdatedAt: time.Now(),
		}
	}

	return Schema{
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
}
